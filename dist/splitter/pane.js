var React = require('react');
var SplitterStyle = require('./splitter.css');

var Pane = React.createClass({
    calculate(){
        var prefixes = ["-webkit-", "-moz-", "-o-", ""];
        var el;
        for (var i = 0; i < prefixes.length; i++) {
            el = document.createElement('div');
            el.style.cssText = "width:" + prefixes[i] + "calc(9px)";
            if (el.style.length) {
                return prefixes[i];
            }
        }
    },
    render() {
        var direction = this.props.direction;

        var prefix = this.calculate().toString();

        var style = {
            border: "1px solid #c0c0c0",
            backgroundColor: this.props.bgColor,
            padding: "20px",
            overflowX: "hidden",
            overflowY: "auto"
        };
        if (direction == 'vertical') {
            style.height = prefix+this.props.size;
        } else {
            style.width = prefix+this.props.size;
            style.height = "100%";
            style.float = "left";
        }

        var className = '';
        if(this.props.dragging){
            className = SplitterStyle.unselectable;
        }

        if(this.props.shadow){
            style.boxShadow = "inset 0 1px 2px #e4e4e4";
        }

        return (<div style={style} className={className}>{this.props.children}</div>);
    }
});

module.exports = Pane;

