var React = require('react');
var SplitterStyle = require('./splitter.css');
var ReactDOM = require('react-dom');
var $ = require('jquery');
var _ = require('lodash');
var Content = require('../Content.js');
var Settings = require('../Settings.js');
var PreviewBox = require('../PreviewBox.js');
var md5 = require('md5');

var TabItem = require('../tabs/item.js');

var Pane = require('./pane');
var Resizer = require('./resizer');

var Splitter = React.createClass({
    getDefaultProps() {
        return {
            //the size of the gutter
            gutterSize: 10,

            //the min size of the first (top or left) pane
            minSizeFirst: 100,

            //the min size of the second (bottom or right) pane
            minSizeSecond: 100,

            //the number of pixels before the minSize to start snap
            snapOffset: 30,

            //direction of the split pane (vertical|horizontal)
            direction: "horizontal",

            //container color
            containerBackground: "#eee",

            //starting size of the first (top or left) pane
            defaultSizeFirst: 50,

            //background color of the panes
            paneBackground: "#ffffff"
        };
    },

    getInitialState() {

        //generate unique hash for this splitter
        this.hash = this._generateHash();

        //get settings from cookie
        var settings = this._getSettings();
        this.settings = _.cloneDeep(settings);

        
        var state = {
            dragging: false,
            currentTab: "content",
        };

        var gutterHalf = this.props.gutterSize / 2;
        state.gutterHalf = gutterHalf;

        state.Asize = settings.Asize;
        state.Bsize = settings.Bsize;

        if (this.props.direction == 'horizontal') {
            state.dimension = 'width'
            state.clientDimension = 'clientWidth'
            state.clientAxis = 'clientX'
            state.position = 'left'
            state.paddingA = 'paddingLeft'
            state.paddingB = 'paddingRight'
        } else if (this.props.direction == 'vertical') {
            state.dimension = 'height'
            state.clientDimension = 'clientHeight'
            state.clientAxis = 'clientY'
            state.position = 'top'
            state.paddingA = 'paddingTop'
            state.paddingB = 'paddingBottom'
        }
        return state;
    },
    componentDidMount() {
        document.addEventListener('mouseup', this.onMouseUp);
        document.addEventListener('mousemove', this.onMouseMove);

        //stop selection from happening
        document.addEventListener('selectstart', this.preventSelection)
        document.addEventListener('dragstart', this.preventSelection)
        document.addEventListener('selectstart', this.preventSelection)
        document.addEventListener('dragstart', this.preventSelection)
    },
    componentDidUpdate() {
        this._saveSettings();
    },
    _saveSettings() {

        var oldSettings = this.settings;
        var newSettings = {
            Asize: this.state.Asize,
            Bsize: this.state.Bsize,
        };


        if (!_.isEqual(oldSettings, newSettings)) {
            this.settings = _.cloneDeep(newSettings);
            // Save to the cookie (will expire in 30 days)
            var someDate = new Date();
            var numberOfDaysToAdd = 30;
            someDate.setDate(someDate.getDate() + numberOfDaysToAdd);
            var expires = someDate.toUTCString(); 

            document.cookie = this.hash + "=" + JSON.stringify(newSettings) + "; expires=" + expires;
           
        }
    },
    read_cookie(name) {
     var result = document.cookie.match(new RegExp(name + '=([^;]+)'));
     result && (result = JSON.parse(result[1]));
     return result;
    },
    getCookie(name) {
        var dc = document.cookie;
        var prefix = name + "=";
        var begin = dc.indexOf("; " + prefix);
        if (begin == -1) {
            begin = dc.indexOf(prefix);
            if (begin != 0) return null;
        }
        else
        {
            begin += 2;
            var end = document.cookie.indexOf(";", begin);
            if (end == -1) {
            end = dc.length;
            }
        }
        return unescape(dc.substring(begin + prefix.length, end));
    }, 
    _getSettings() {
        //var cookie = document.cookie;

        var cookie = this.getCookie(this.hash);
        var settings = {};
        if (cookie == null) {
            var gutterHalf = this.props.gutterSize / 2;

            //size
            if(this.props.defaultSizeFirst){
                var Asize = 'calc(' + this.props.defaultSizeFirst + '% - ' + (41 + gutterHalf) + 'px)';
                var Bsize = 'calc(' + (100 - this.props.defaultSizeFirst) + '% - ' + gutterHalf + 'px)'
            }else{
                var Asize = 'calc(50% - ' + (41 + gutterHalf) + 'px)';
                var Bsize = 'calc(50% - ' + gutterHalf + 'px)';
            }

            settings = {
                Asize: Asize,
                Bsize: Bsize
            };
        }else{
            settings = this.read_cookie(this.hash);
        }

        return settings;
    },
    _generateHash() {
        // Generate unique hash for the page+splitter
        var str = window.location.hostname + window.location.pathname + this.props.name;
        var hash = md5(str);

        return 'splitter_' + hash;
    },
    preventSelection(){
        return false;
    },
    componentWillUnmount() {
        document.removeEventListener('mouseup', this.onMouseUp);
        document.removeEventListener('mousemove', this.onMouseMove);
    },
    onMouseDown(event) {
        this.setState({
            dragging: true,
        });

    },
    onMouseMove(event) {
        if (this.state.dragging) {
            event.preventDefault();

            // Calculate the pairs size, and percentage of the parent size
            var splitter = ReactDOM.findDOMNode(this.refs.splitter);
            var a = ReactDOM.findDOMNode(this.refs.first);
            var b = ReactDOM.findDOMNode(this.refs.second);
            var computedStyle = global.getComputedStyle(splitter);
            var parentSize = splitter[this.state.clientDimension] - parseFloat(computedStyle[this.state.paddingA]) - parseFloat(computedStyle[this.state.paddingB]);
            var size = a.getBoundingClientRect()[this.state.dimension] + b.getBoundingClientRect()[this.state.dimension] + this.state.gutterHalf + this.state.gutterHalf;
            var percentage = Math.min(size / parentSize * 100, 100);
            var start = a.getBoundingClientRect()[this.state.position];

            var offset = event[this.state.clientAxis] - start

            // If within snapOffset of min or max, set offset to min or max

            if (offset <=  this.props.minSizeFirst + this.props.snapOffset) {
                offset = this.props.minSizeFirst;
            } else if (offset >= size - this.props.minSizeSecond - this.props.snapOffset) {
                offset = size - this.props.minSizeSecond 
            }

            this.setState({
                Asize: 'calc(' + (offset / size * percentage) + '% - ' + this.state.gutterHalf + 'px)',
                Bsize: 'calc(' + (percentage - (offset / size * percentage)) + '% - ' + this.state.gutterHalf + 'px)'
            });
           
        }
    },
    onMouseUp() {
        this.setState({
            dragging: false
        });
    },
    changeCurrent(content){
        //$(ReactDOM.findDOMNode(this.refs.splitter)).trigger("click"); 
        this.props.onChange(content);
    },
    _onSelectTab(tab) {
        this.setState({
            currentTab: tab,
        });
    },
    render() {

        var direction = this.props.direction;
        var splitter = {
            backgroundColor: this.props.containerBackground, 
            height: "inherit", 
            padding: "10px", 
            border: "1px solid #c0c0c0"
        };

        var paneStyle = {
            backgroundColor: this.props.paneBackground, 
        };


        var children = this.props.children;
        if(this.props.direction == "vertical"){
            var paneClass = "vertical";
            var gutterClass = "gutter gutter-vertical "+SplitterStyle.gutterVertical;
            var gutterStyle = {
                cursor: "ns-resize",
                height: this.props.gutterSize
            };
        }else{
            var paneClass = "horizontal";
            paneStyle.float = "left";
            paneStyle.height = "100%";
            var gutterClass = "gutter gutter-horizontal "+SplitterStyle.gutterHorizontal;
            var gutterStyle = {
                cursor: "ew-resize",
                width: this.props.gutterSize,
                float: "left",
                height: "100%"
            };
        }

        var tabList = [
            {name: 'content', title: 'Content', content: <Content onContentChange={this.changeCurrent} serverObj={this.props.obj} />},
            {name: 'settings', title: 'Settings', content: <Settings serverObj={this.props.obj} />},
        ];

        var tabContent = '';
        var rows = tabList.map((item) => {
            if (this.state.currentTab === item.name) {
                tabContent = item.content;
            }

            return (
                <TabItem
                    key={item.name}
                    name={item.name}
                    title={item.title}
                    badge={item.badge}
                    isActive={this.state.currentTab === item.name}
                    onSelectTab={this._onSelectTab}
                />
            );
        });

        var classNameBtn = "btn btn-primary";

        return (
             <div ref="splitter" className="splitter" style={splitter}>
                <div style={{height: "inherit", width: "inherit", position: "relative"}} ref="splitPane">
                    <ul className={'nav nav-tabs'} style={{marginBottom: "10px"}}>
                        {rows}
                    </ul>
                    <Pane shadow={false} dragging={this.state.dragging} ref="first" size={this.state.Asize} bgColor={this.props.paneBackground} direction={this.props.direction} className={paneClass}>{tabContent}</Pane>
                    <div className={gutterClass} style={gutterStyle} onMouseDown={this.onMouseDown}></div>
                    <Pane shadow={true} dragging={this.state.dragging} ref="second" size={this.state.Bsize} bgColor={this.props.paneBackground} direction={this.props.direction} className={paneClass}>
                      <PreviewBox content={this.props.obj.content} stepType={this.props.obj.type} />
                    </Pane>
                </div>
            </div>
        );
    },
});


module.exports = Splitter;
