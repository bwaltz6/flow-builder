var React = require('react');
var SplitterStyle = require('./splitter.css');
var ClickButton = require('cccisd-click-button');

var Resizer = React.createClass({
    onMouseDown(event) {
        this.props.onMouseDown(event);
    },
     _AddSaveButton: function(){
        var className = "btn btn-primary";
        return (
            <button
                style={{
                    position: "absolute",
                    top: "50%",
                    marginLeft: "15px",
                    transform: "translateY(-50%)"
                }}
                className={className}
                onClick={this.props.onSubmit}
            >Publish Step</button>
        );
    },
    render() {
    	if(this.props.direction == 'horizontal'){
    		var className = SplitterStyle.Resizer + ' ' + SplitterStyle.horizontal;
    	}else{
    		var className = SplitterStyle.Resizer + ' ' + SplitterStyle.vertical;
    	}

        var style ={
            height: this.props.size,
            position: "relative"
        };

        var saveBtn = this._AddSaveButton();
        
        return (
            <div className={className} style={style} onMouseDown={this.onMouseDown}>
                {saveBtn}
            </div>
        );
    }
});

module.exports = Resizer;