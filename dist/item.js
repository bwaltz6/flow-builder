var Tooltip = require('cccisd-tooltip');
var React = require('react');
var ReactDOM = require('react-dom');
var _ = require('lodash');
var $ = require('jquery');
var toolbarStyle = require('./nodeToolbar.css');

var ItemTypes = require('./Constants').ItemTypes;
import {DragSource} from 'react-dnd';

var itemSource = {
  beginDrag: function (props, component) {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for( var i=0; i < 10; i++ ){
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    }

    return { id: props.id,  tmpId: text };
  }
};

function collectSource(connect, monitor) {
  return {
    connectDragSource: connect.dragSource(),
    isDragging: monitor.isDragging()
  }
}

var Item = React.createClass({
    propTypes: {
        connectDragSource: React.PropTypes.func.isRequired,
        isDragging: React.PropTypes.bool.isRequired
    },
    _makeTmpId(){
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        for( var i=0; i < 10; i++ )
            text += possible.charAt(Math.floor(Math.random() * possible.length));

        return text;
    },
    render() {

        var connectDragSource = this.props.connectDragSource;
        var isDragging = this.props.isDragging;

        var nodeClass = 'list-group-item ' +toolbarStyle.node;
        var itemAttributes = this.props.itemAttributes;

        var nodeTitle = this.props.nodeTitle.toUpperCase().replace(/ /g,"_");
        var iconClass = itemAttributes[nodeTitle].icon;
        
        var style={
            backgroundColor: itemAttributes[nodeTitle].color
        };

        return connectDragSource(
            <div style={{marginBottom: "15px"}}>
                <Tooltip placement="right" title={nodeTitle}>
                <li data-type={this.props.nodeTitle} key={this.props.nodeTitle} className={nodeClass} style={style}>    
                    <div>
                        <i className={iconClass}></i>
                    </div>    
                </li> 
                </Tooltip>
            </div>
        );
    },
});

module.exports = DragSource(ItemTypes.NODEITEM, itemSource, collectSource)(Item);
