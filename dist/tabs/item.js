var React = require('react');

var Item = React.createClass({
    propTypes: {
        name:        React.PropTypes.string,
        title:       React.PropTypes.string,
        badge:       React.PropTypes.node,
        isActive:    React.PropTypes.bool,
        onSelectTab: React.PropTypes.func,
    },
    getDefaultProps() {
        return {
            name: '',
            title: '',
            badge: '',
            isActive: false,
            onSelectTab: function() {},
        };
    },
    _clicked(e) {
        e.preventDefault();
        this.props.onSelectTab(this.props.name);
    },
    render() {
        var className = this.props.isActive ? 'active' : '';
        var badge = (this.props.badge === '') ? null : <span className="badge">{this.props.badge}</span>;

        return (
            <li role="presentation" className={className} onClick={this._clicked}>
                <a href="#">{this.props.title + ' '}{badge}</a>
            </li>
        );
    },
});

module.exports = Item;
