var React   = require('react');
var TabItem = require('./item.js');

var style   = require('./style.css');


var Tabs = React.createClass({
    propTypes: {
        tabList:   React.PropTypes.array,
        saveInHash:   React.PropTypes.bool,
    },
    getDefaultProps() {
        return {
            // Array with the structure {name: ???, title: ???, content: ???}
            tabList: [],

            // Save the current tab in the URL hash or not?
            saveInHash: false,
        };
    },
    getInitialState() {
        var tab = this.props.tabList[0].name;

        if (this.props.saveInHash) {
            if (window.location.hash !== '') {
                tab = window.location.hash.substring(1);
            }

            window.location.hash = tab;
        }

        return {
            // set the first tab as default
            currentTab: tab,
        };
    },
    _onSelectTab(tab) {
        if (this.props.saveInHash) {
            window.location.hash = tab;
        }

        this.setState({
            currentTab: tab,
        });
    },
    onSave(){
        this.props.onSave();
    },
    render() {
        if (this.props.tabList.length === 0) {
            return null;
        }

        var tabContent = '';
        var rows = this.props.tabList.map((item) => {
            if (this.state.currentTab === item.name) {
                tabContent = item.content;
            }

            return (
                <TabItem
                    key={item.name}
                    name={item.name}
                    title={item.title}
                    badge={item.badge}
                    isActive={this.state.currentTab === item.name}
                    onSelectTab={this._onSelectTab}
                />
            );
        });

        /*var className = "btn btn-primary";
        <div style={{float: "right"}}>
                    <button onClick={this.onSave} style={{marginRight: "15px"}} className={className}>Save</button>
                    <button onClick={this.props.onPublish} className={className}>Publish</button>
                    </div>
        */

        return (
            <div>
                <ul className={'nav nav-tabs ' + style.tabs}>
                    {rows}
                </ul>
                {tabContent}
            </div>
        );
    },
});

module.exports = Tabs;
