var React = require('react');
var ReactDOM = require('react-dom');
var _ = require('lodash');
var $ = require('jquery');

var ZoouForm = React.createClass({
  render() {
    var content = this.props.contentObject.content;
    return(
      <div>
        <div style={{marginBottom: "20px"}}>
          <label>Zoo U Settings</label>
          <div>No configuration needed</div>
        </div>
      </div>
    );
  },
});


module.exports = ZoouForm;
