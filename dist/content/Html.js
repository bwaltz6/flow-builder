var React = require('react');
var ReactDOM = require('react-dom');
var _ = require('lodash');
var $ = require('jquery');
var Winterfell = require('winterfell');

import TinyMCE from 'react-tinymce';

//Winterfell.addInputType('MyType', MyType);
 
var Html = React.createClass({
  getInitialState() {
    var FormFormat = {  
       "classes":{  
          "form":"login-form",
          "question":"form-group",
          "input":"form-control",
          "controlButton":"btn btn-primary pull-right",
          "backButton":"btn btn-default pull-left",
          "errorMessage":"alert alert-danger",
          "buttonBar":"button-bar"
       },
       "formPanels":[  
          {  
             "index":1,
             "panelId":"html-panel"
          }
       ],
       "questionPanels":[  
          {  
             "panelId":"html-panel",
             "panelHeader":"Html content",
             "panelText":"Please configure the content for this HTML step:",
             "action":{  
                "default":{  
                   "action":"SUBMIT"
                }
             },
             "button":{  
                "text":"Submit"
             },
             "questionSets":[  
                {  
                   "index":1,
                   "questionSetId":"html-set"
                }
             ]
          }
       ],
       "questionSets":[  
          {  
             "questionSetId":"html-set",
             "questions":[  
                {  
                   "questionId":"player title",
                   "question":"Player Title",
                   "input":{  
                      "type":"textInput",
                      "default": this.props.contentObject.content.player_title,
                      "placeholder": this.props.contentObject.content.player_title
                   },
                   "validateOn":"blur",
                    "validations":[  
                      {  
                         "type":"isLength",
                         "params":[1]
                      }
                   ]
                },
                {  
                   "questionId":"content",
                   "question":"HTML Content",
                   "input":{  
                      "type":"MyType",
                      "placeholder": "this is a placeholder",
                      "default": this.props.contentObject.content
                   },
                   "validations":[  
                      {  
                         "type":"isLength",
                         "params":[1]
                      }
                   ]
                }
             ]
          }
       ]
    };

    return {
      //timer: 0,
      formSchema: FormFormat,
    };
  },
  handleEditorChange(e) {
    this.props.onContentChange( {html: e.target.innerHTML} );
  },
  handleEditorNodeChange(e) {
    this.props.onContentChange( {html: e.target.getContent()} );
  },
  handleTitleChange: function(e) {
    this.props.onContentChange( {player_title: e.target.value} );
  },
  render() {
    /*return(
       <Winterfell schema={this.state.formSchema} />
    );*/
    var titleVal = this.props.contentObject.content.player_title;
    var contentVal = "";
    if ( this.props.contentObject.content ) {
        contentVal = this.props.contentObject.content.html;
    }
   return(
      <div>
        <div style={{marginBottom: "20px"}}>
          <label>Player Title</label>
          <input className="form-control" type="text" value={titleVal} onChange={this.handleTitleChange} />
        </div>
        <div>
          <label>Content</label>
          <TinyMCE
            ref="editor"
            content={contentVal}
            config={{
              plugins: 'autolink link image lists print preview',
              toolbar: 'undo redo | bold italic | alignleft aligncenter alignright'
            }}
            onKeyup={this.handleEditorChange}
            onNodeChange={this.handleEditorNodeChange}
          />
        </div>
      </div>
    );
  },
});


module.exports = Html;
