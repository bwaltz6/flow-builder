var React = require('react');
var ReactDOM = require('react-dom');
var _ = require('lodash');
var $ = require('jquery');

var QuizshowForm = require("./QuizshowForm.js");
var ZoouForm = require("./ZoouForm.js");

var Game = React.createClass({
  handleChange(e) {
    var newContent = {};
    newContent[e.target.name] = e.target.value;
    this.props.onContentChange( newContent );
  },
  render() {
   var content = this.props.contentObject.content;
   return(
      <div>
        <div style={{marginBottom: "20px"}}>
          <label>Player Title</label>
          <input className="form-control" type="text" name="player_title" value={content.player_title} onChange={this.handleChange} />
        </div>
        <div style={{marginBottom: "20px"}}>
          <label>Game Type</label>
          <select className="form-control" name="gameType" onChange={this.handleChange} value={content.gameType}>
             <option value="">Choose a game...</option>
             <option value="quizshow">Quiz Show</option>
             <option value="zoou">Zoo U</option>
          </select>
        </div>
        <div style={{marginBottom: "20px", display: (content.gameType=="quizshow") ? "block" : "none" }}>
          <QuizshowForm 
            onChange={this.handleChange}
            contentObject={this.props.contentObject}
          />
        </div>
        <div style={{marginBottom: "20px", display: (content.gameType=="zoou") ? "block" : "none" }}>
          <ZoouForm 
            onChange={this.handleChange}
            contentObject={this.props.contentObject}
          />
        </div>
      </div>
    );
  },
});

/*
*/

module.exports = Game;
