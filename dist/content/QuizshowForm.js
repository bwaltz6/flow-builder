var React = require('react');
var ReactDOM = require('react-dom');
var _ = require('lodash');
var $ = require('jquery');

var QuizshowForm = React.createClass({
  render() {
    var content = this.props.contentObject.content;
    return(
      <div>
        <div style={{marginBottom: "20px"}}>
          <h4>Quiz Show Configuration</h4>
          <label>Quiz ID</label>
          <select onChange={this.props.onChange} className="form-control" name="quizshow_quizId" value={content.quizshow_quizId}>
             <option value="">Choose a quiz</option>
             <option value="one">One</option>
             <option value="two">Two</option>
          </select>
        </div>
      </div>
    );
  },
});


module.exports = QuizshowForm;
