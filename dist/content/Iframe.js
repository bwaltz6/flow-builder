var React = require('react');
var ReactDOM = require('react-dom');
var _ = require('lodash');
var $ = require('jquery');

var Style = require('../style.css');

var Iframe = React.createClass({
  handleChange(e) {
    var newContent = {};
    newContent[e.target.name] = e.target.value;
    this.props.onContentChange( newContent );
  },

  render() {
    var content = this.props.contentObject.content;
    return(
      <div>
        <div style={{marginBottom: "20px"}}>
          <label>Player Title</label>
          <input className="form-control" type="text" name="player_title" value={content.player_title} onChange={this.handleChange} />
        </div>
        <div style={{marginBottom: "20px"}}>
          <label>IFRAME</label>
          <input className="form-control" type="text" name="iframe" placeholder="<IFRAME src=&quot;&quot;></IFRAME>" value={content.iframe} onChange={this.handleChange} />
        </div>
      </div>
    );
  },
});


module.exports = Iframe;
