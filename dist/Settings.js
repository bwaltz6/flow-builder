var React = require('react');
var _ = require('lodash');
var $ = require('jquery');
var ClickButton = require('cccisd-click-button');
var Tabs = require('cccisd-tabs');
var StepEditorStyle = require('./stepEditor.css');

//var Html = require('./settings/html.js');

var Settings = React.createClass({
    render() {

    	var contentClass = StepEditorStyle.content + ' '+ StepEditorStyle.text;

    	/*if(this.props.serverObj && !_.isEmpty(this.props.serverObj)){
	        switch(this.props.serverObj.type){
	        	case 'html':
	        		return (<div className={StepEditorStyle.content}><HTML contentObject={this.props.serverObj} /></div>);
	        		break;
	        }
	    }else{*/
	    	return (<div className={contentClass}>No settings at this time</div>);
	    //}
    },
});

module.exports = Settings;
