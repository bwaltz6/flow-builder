var React = require('react');
var _ = require('lodash');
var $ = require('jquery');
var StepEditorStyle = require('./stepEditor.css');

var Html = require('./content/Html.js');
var Game = require('./content/Game.js');
var Iframe = require('./content/Iframe.js');

var Content = React.createClass({
    render() {
    	var contentClass = StepEditorStyle.content + ' '+ StepEditorStyle.text;

    	if(this.props.serverObj && !_.isEmpty(this.props.serverObj)){
	        switch(this.props.serverObj.type){
	        	case 'html':
	        		return (<div className={StepEditorStyle.content}><Html onContentChange={this.props.onContentChange} contentObject={this.props.serverObj} /></div>);
	        		break;

	        	case 'game':
	        		return (<div className={StepEditorStyle.content}><Game onContentChange={this.props.onContentChange} contentObject={this.props.serverObj} /></div>);
	        		break;

	        	case 'iframe':
	        		return (<div className={StepEditorStyle.content}><Iframe onContentChange={this.props.onContentChange} contentObject={this.props.serverObj} /></div>);
	        		break;
	        	default:
	        		return (<div className={StepEditorStyle.content}></div>);
	        		break;
	        }
        }
        return (<div className={contentClass}></div>);
    },
});

module.exports = Content;
