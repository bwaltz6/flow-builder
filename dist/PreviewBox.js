var React = require('react');
var _ = require('lodash');
var $ = require('jquery');
var Loader = require('cccisd-loader');

var Html = require('./play/Html.js');
var Game = require('./play/Game.js');
var Iframe = require('./play/Iframe.js');

var PreviewBox = React.createClass({
    render() {
        if (this.props.content && !_.isEmpty( this.props.content ) ){
            switch( this.props.stepType ) {
                case 'html':
                    return(
            	        <div className="preview"><Html content={this.props.content} />
    	                </div>
    	            );
                    break;
                case 'game':
                    return(
            	        <div className="preview"><Game content={this.props.content} />
    	                </div>
    	            );
                    break;
                case 'iframe':
                    return(
            	        <div className="preview"><Iframe content={this.props.content} />
    	                </div>
    	            );
                    break;
                default:
                    return(
            	        <div className="preview">
    	                </div>
    	            );
                    break;
            }
        }
        return(
            <div className="preview">
            </div>
        );
    },
});

module.exports = PreviewBox;
