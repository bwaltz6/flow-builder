var React = require('react');
var ReactDOM = require('react-dom');
var _ = require('lodash');
var $ = require('jquery');
var ClickButton = require('cccisd-click-button');

var nodeStyle = require('./nodeItem.css')

var ItemTypes = require('./Constants').ItemTypes;
import {DragSource, DropTarget} from 'react-dnd';

var nodeitemSource = {
  beginDrag: function (props, monitor, component) {
    return { id: props.item.id, title: props.item.outline_title, height: ReactDOM.findDOMNode(component).offsetHeight };
  }
};

var target = {
    hover(props, monitor, component) {
        // Determine rectangle on screen
        var hoverBoundingRect = ReactDOM.findDOMNode(component).getBoundingClientRect();

        // Get horizontal middle
        var hoverMiddleY = (hoverBoundingRect.bottom - hoverBoundingRect.top) / 2;

        // Determine mouse position
        var clientOffset = monitor.getClientOffset();

        // Get pixels to the left
        var hoverClientY = hoverBoundingRect.bottom - clientOffset.y;

        if (hoverClientY > hoverMiddleY) {
            props.movePlaceholderBefore(props.id);
        } 
        else {
            props.movePlaceholderAfter(props.id);
        }
    },
};

function collectSource(connect, monitor) {
  return {
    connectDragSource: connect.dragSource(),
    isDragging: monitor.isDragging()
  }
}

function collectTarget(connect, monitor) {
  return {
    connectDropTarget: connect.dropTarget()
  }
}

var NodeItem = React.createClass({

	propTypes: {
        nodeClass: React.PropTypes.string,
        connectDragSource: React.PropTypes.func.isRequired,
        isDragging: React.PropTypes.bool.isRequired
    },
    getDefaultProps() {
        return {
        	nodeClass: '',
        }
    },
    getInitialState() { 
        return {
            isHovering: false,
            nodeTitleCopy: this.props.item.outline_title,
            nodeTitle: this.props.item.outline_title,
            editing: false,
            deleted: false,
        };
    },
    handleMouseOver: function () {
        this.setState({ isHovering: true });
    },
    handleMouseOut: function() {
        this.setState({ isHovering: false });
    },
    _deleteButton: function(item){
        var className = "glyphicon glyphicon-remove";
        var item = this.props.item;
        return (
            <ClickButton
                title=''
                className={className}
                isConfirm={false}
                onClick={this.onDeleteClick.bind(this, item)}
            />
        );
    },
    _revertButton: function(item){
        var className = "glyphicon glyphicon-repeat";
        var item = this.props.item;
        return (
            <ClickButton
                title=''
                className={className}
                isConfirm={false}
                onClick={this.onRevertClick.bind(this, item)}
            />
        );
    },
    _confirmEditButton: function(item){
        var className = "glyphicon glyphicon-ok";
        var item = this.props.item;
        return (
            <ClickButton
                title=''
                className={className}
                isConfirm={false}
                onClick={this.handleBlur}
            />
        );
    },
    onEditClick: function(clickedItem){
        if(!this.state.editing){
            this.props.onEdit(clickedItem);
        }
    },
    onDeleteClick: function(clickedItem){
        this.setState({ deleted: true });
        this.props.onDelete(clickedItem);
    },
    onRevertClick: function(clickedItem){
        this.setState({ deleted: false });
        this.props.onDeleteRevert(clickedItem);
    },
    onChange: function(event) {
        this.setState({nodeTitle: event.target.value});
    },
    enableRename: function(){

        this.setState({ 
            editing: true,
            nodeTitleCopy: this.state.nodeTitle,
        });

        this.props.onRenaming(this.props.item);

    },
    handleBlur: function(){

        //only if title text has changed
        if(this.state.nodeTitleCopy != this.state.nodeTitle){

            //set editing true
            this.setState({ editing: false });

            this.props.onRenameSubmit(this.props.item, this.state.nodeTitle);
        }
        else{
            this.setState({ editing: false});
        }
    },
    getClickHandler(onClick, onDblClick, delay) {
        var timeoutID = null;
        delay = delay || 250;
        if (this.state.deleted){
            return false;
        }
        else{
            return function (event) {
                if (!timeoutID) {
                    timeoutID = setTimeout(function () {
                        onClick(event);
                        timeoutID = null
                    }, delay);
                } else {
                    timeoutID = clearTimeout(timeoutID);
                    onDblClick(event);
                }
            };
        }
    },
    inputEscape(){
        this.setState({
            nodeTitle: this.state.nodeTitleCopy,
            editing: false
        });

        this.props.onRenameCancel(this.props.item);
    },
    onKey(e){

        var key = e.keyCode;

        if(key == 13){
            //enter
            this.handleBlur();
        }
        else if(key == 27){
            //escape
            this.inputEscape(); 
        }
        
    },
    render() {

        var connectDragSource = this.props.connectDragSource;
        var connectDropTarget = this.props.connectDropTarget;
        var isDragging = this.props.isDragging;

        if(isDragging){
            return null;
        }

        var changed = this.props.item.changed ? nodeStyle.changed : null;
        var active = this.props.active ? nodeStyle.active : null;

        var hoverClass = '';
        if(this.state.isHovering){
            hoverClass += nodeStyle.hover;
        }

        //if deleted than toggle opacity class and buttons
        var deleted = '';
        if(this.state.deleted){
            deleted = nodeStyle.deleted;
            var revertClass = 'glyphicon glyphicon-repeat';
            var buttons = this._revertButton(this.props.item);

        }else if(this.state.editing){
            var buttons = this._confirmEditButton();
        }
        else{
            var buttons = this._deleteButton(this.props.item);
        }

        var nodeClass = 'list-group-item ' +nodeStyle.item + ' '+ changed +' '+deleted +' '+active;

        var nodeType = this.props.item.type.toUpperCase().replace(/ /g,"_");

        //get icons and styles from node attributes
        var iconClass = '';
        if(this.props.itemAttributes[nodeType].icon){
            iconClass += this.props.itemAttributes[nodeType].icon;
        }
        var style = {};
        if(this.props.itemAttributes[nodeType].color){
            style.backgroundColor = this.props.itemAttributes[nodeType].color
        }

        if(this.state.editing){
            var inputStyle = {
               backgroundColor: '#ffffff',
               resize: "none",
               overflow: "hidden",
               display: "block",
               width: "100%"
           };
            return (
                <li 
                    data-type={this.props.item.type} 
                    key={this.props.item.id} 
                    data-nodeid={this.props.item.id} 
                    className={nodeClass} 
                    onMouseOver={this.handleMouseOver} 
                    onMouseOut={this.handleMouseOut} 
                    
                >
                    <div style={{display: "table-row"}}>
                        <div style={{backgroundColor: this.props.itemAttributes[nodeType].color}}>
                            <div style={{fontSize: "10px", lineHeight: "5px"}}>{this.props.index}</div>
                            <div>
                                <i className={iconClass}></i>
                            </div>
                        </div>
                        <div className="title" onClick={this.getClickHandler(this.onEditClick.bind(null, this.props.item), this.enableRename)}>
                            <input
                                ref="editable"
                                type="text" 
                                className="form-control"
                                style={inputStyle} 
                                autoFocus={true}
                                value={this.state.nodeTitle} 
                                onChange={this.onChange} 
                                onKeyDown={this.onKey}
                            />
                        </div>
                        <div className={hoverClass}>
                            <div>{buttons}</div>
                        </div>
                    </div>
                </li>
            );
        }
        else{
            return connectDragSource(connectDropTarget(
                <li 
                    data-type={this.props.item.type} 
                    key={this.props.item.id} 
                    data-nodeid={this.props.item.id} 
                    className={nodeClass} 
                    onMouseOver={this.handleMouseOver} 
                    onMouseOut={this.handleMouseOut}    
                >
                    <div style={{display: "table-row"}}>
                        <div style={{backgroundColor: this.props.itemAttributes[nodeType].color}}>
                            <div style={{fontSize: "10px", lineHeight: "5px"}}>&nbsp;{this.props.index}</div>
                            <div>
                                <i className={iconClass}></i>
                            </div>
                        </div>
                        <div className="title" onClick={this.getClickHandler(this.onEditClick.bind(null, this.props.item), this.enableRename)}>
                            {this.state.nodeTitle}
                        </div>
                        <div className={hoverClass}>
                            <div>{buttons}</div>
                        </div>
                    </div>
                </li>
            ));
        }
    },
});


module.exports = DropTarget(ItemTypes.NODEITEM, target, collectTarget)(DragSource(ItemTypes.NODEITEM, nodeitemSource, collectSource)(NodeItem));
