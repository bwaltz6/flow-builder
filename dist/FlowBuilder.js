//contributed
var React = require('react');
var ReactDOM = require('react-dom');
var $ = require('jquery');
var _ = require('lodash');
var DragDropContext = require('react-dnd').DragDropContext;
var HTML5Backend = require('react-dnd-html5-backend');

//local
var Menu = require('./menu');
var NodeToolbar = require('./nodeToolbar');
var Style = require('./style.css');
var StepEditor = require('./stepEditor.js');
var StepEditorStyle = require('./stepEditor.css');
var Splitter = require('./splitter/splitter.js');

//3C
var ClickButton = require('cccisd-click-button');
var Loader = require('cccisd-loader');

//3C package
//var Quest = window.cccisd.Quest;
var Flow = window.cccisd.flow;

var FlowBuilder = React.createClass({
  getInitialState() {
      return {
        loading: true,
        menuData: {},
        changes: {
          update: [],
          delete: [],
        },
        appMenuData: {},
        toolbarData: [],
        activeNode: {},
        contentDirty: false,
        tmpItem: {}
      };
  },
  componentDidMount: function () {
     var $this = this;

    //load node toolbar
    this._loadToolbar();

    //update the menu
    this._update();

  },
  _loadToolbar: function() {
    var $this = this;

    $this.setState({
      loading: true,
    });

    $.ajax({
      type: 'GET',
      url: Flow.route('flow.node.types'),
      data: {},
      dataType: 'json',
      success(result) {
        $this.setState({
          toolbarData: result,
          loading: false,
        });
      },
    });
  },
  _update: function(){
    var $this = this;

    $this.setState({
      loading: true,
    });

    //inital ajax of outline data
    //TODO: get id from somewhere else instead of hardcoding
    $.ajax({
      type: 'GET',
      url: Flow.route('flow.node.outline').replace('/{root?}', ''),
      data: {},
      dataType: 'json',
      success(result) {

        $this.setState({
          menuData: result.data[0].children[0],
          appMenuData: result.data[0].children[0],
        });

        if(!_.isEmpty($this.state.tmpItem)){
          $this._loadContent($this.state.tmpItem);
        }

        $this.setState({
          loading: false,
        });
      },
    });
  },
  _updatePositions: function(startIndex, endIndex, increment){
    for(var i = startIndex; i <= endIndex; i++){
      this.state.appMenuData.children[i].position = i;

      if(this._inArray(this.state.changes.update, this.state.appMenuData.children[i])){
        var updateItem = this._getObjectById(this.state.changes.update, this.state.appMenuData.children[i].id);
        updateItem.position = i;
      }else{
        var updateNode = {
          id: this.state.appMenuData.children[i].id,
          outline_title: this.state.appMenuData.children[i].outline_title,
          position: i,
        };
        this.state.changes.update.push(updateNode);
      }
    }
  },
  _getNodeIndex: function(id, arr){
    
    //find index of element in appMenuData
    var elementIndex = arr.map(function(x) {return x.id; }).indexOf(id);
    return elementIndex;
  },
  _inArray: function(array, obj){

      var foundItem = this._getObjectById(array, obj.id);

      if(foundItem){
        return foundItem;
      }
      else{
        return false;
      }
  },
  _getNodeByIndex: function(el, arr){
    //find index of element in changes create array
    var elementIndex = arr.map(function(x) {return x.id; }).indexOf(el.id);
    return elementIndex;
  },
  _getObjectById: function(arr, id){
    // (!) Cache the array length in a variable
    for (var i = 0, len = arr.length; i < len; i++) {
        if (arr[i].id === id)
          return arr[i]; // Return as soon as the object is found
    }
    
    return null; // The searched object was not found
  },
  _updateChange: function(bundle){
    var $this = this;
    if(!_.isEmpty(bundle.data.resources)){
      return $.ajax({
        type: 'PUT',
        url: Flow.route('flow.nodes.batch').replace('/{ids}', ''),
        data: bundle,
        dataType: 'json',
        success(result) {
        },
      });
    }
    else{
      return true;
    }
  },
  _deleteChange: function(ids){
    var $this = this;
    if(!_.isEmpty(ids)){
      return $.ajax({
        type: 'DELETE',
        url: Flow.route('flow.nodes.batch').replace('{ids}', ids.join()),
        dataType: 'json',
        success(result) {
        },
      });
    }else{
      return true;
    }
  },
  _saveContentSettings(contentObj){
    var $this = this;
    var contentJson = {
      "data": {
        "type": "node",
        "attributes": {
            "content": JSON.stringify(contentObj.content)
        }
      }
    };
    if(!_.isEmpty(contentObj)){
      return $.ajax({
        type: 'PUT',
        data: contentJson,
        url: Flow.route('flow.node.content.show').replace('{node}', contentObj.id).replace('{lang?}', "en-us"),
        dataType: 'json',
        success(result) {
        },
      });
    }else{
      return true;
    }
  },
  _savePlayerTitle(contentObj){
    var $this = this;
    var updateBundle = {
      data: {
        resources: [
          {
            type: "node",
            attributes: {
              player_title: contentObj.content.player_title,
              id: contentObj.id
            }
          }
        ]
      }
    };

    return $.ajax({
      type: 'PUT',
      url: Flow.route('flow.nodes.batch').replace('/{ids}', ''),
      data: updateBundle,
      dataType: 'json',
      success(result) {
      },
    });
   
  },
  _saveChanges: function(){
    var $this = this;
    $(this.refs.modal).modal('hide');
    //bundle up create changes
    var updateBundle = {
      data: {
        resources: [
        ]
      }
    };

    $.each($this.state.changes.update, function( index, node ) {
      if(node.new){
        var batchNode = {
          "type": "node",
          tmpId: node.id,
          attributes: {
            outline_title: node.outline_title,
            parent_id: node.parent_id,
            position: node.position,
            type: node.type,
          }
        };
      }else{
        var batchNode = {
          "type": "node",
        };

        batchNode.attributes = node;
      }
    
      //push to bundle
      updateBundle.data.resources.push(batchNode);
    });

    //bundle up create changes
    var deleteIds = [];

    if(!_.isEmpty($this.state.changes.delete)){
      $.each($this.state.changes.delete, function( index, node ) {

        //push to bundle
        deleteIds.push(node.id);

      });
    }

    $this.setState({
      changes: {
        update: [],
        delete: [],
      },
    });

    //if the active node is new
    if (!_.isEmpty(this.state.activeNode)) {
        //are we trying to delete the active node?
        if(this._inArray(this.state.changes.delete, this.state.activeNode)){

          //make requests
          $.when(
            this._updateChange(updateBundle),
            this._deleteChange(deleteIds)
          ).done(function() {

            $this.setState({
              contentDirty: false,
              activeNode: {},
            });
            
            $this._update(); 
         
          });
        }  
        else if (this.state.activeNode.new ) {

            //make requests
            $.when(
              this._updateChange(updateBundle),
              this._deleteChange(deleteIds)
            ).done(function(data, textStatus, jqXHR) {
              for (var i = 0, len = data[0].data.resources.length; i < len; i++) {
                if (data[0].data.resources[i].tmpId === $this.state.activeNode.id){
                   var newId =  data[0].data.resources[i].attributes.id;
                }
              }
              var compositeItem = _.cloneDeep($this.state.activeNode);
              compositeItem.id = newId;
              $this._saveContentSettings(compositeItem);
              $this._savePlayerTitle(compositeItem);

              //var item = _.cloneDeep($this.state.tmpItem);

              $this.setState({
                contentDirty: false,
              });
            
              /*if(!_.isEmpty(item)){
              $this.setState({
                activeNode: item
              });
            }*/
            
            $this._update();
            });
        } else {

          //make requests
          $.when(
            this._saveContentSettings(this.state.activeNode),
            $this._savePlayerTitle(this.state.activeNode),
            this._updateChange(updateBundle),
            this._deleteChange(deleteIds)
          ).done(function() {
            //var item = _.cloneDeep($this.state.tmpItem);

            $this.setState({
                contentDirty: false,
                //tmpItem: {}
              });
            
              /*if(!_.isEmpty(item)){
              $this.setState({
                activeNode: item
              });
            }*/
            
            $this._update();
          });
        }
    } else {
      $.when(
          this._updateChange(updateBundle),
          this._deleteChange(deleteIds)
      ).done(function() {
          $this._update();
      });
    }
  },
  _AddSaveButton: function(){
      if((_.isEmpty(this.state.changes.update) && this.state.changes.update) && (_.isEmpty(this.state.changes.delete) && this.state.changes.delete) && !this.state.contentDirty){
        var disabled = true;
      }else{
        var disabled = false;
      }

      var className = "btn btn-primary "+Style.savebtn;
      return (
          <ClickButton
              title='Save'
              className={className}
              isConfirm={false}
              onClick={this._saveChanges}
              isDisabled={disabled}
          />
      );
  },
  _AddDiscardChangesButton: function(){
      var $this = this;
      var className = "btn btn-primary";
      return (
          <ClickButton
              title='Discard Changes'
              className={className}
              isConfirm={false}
              onClick={function(){$this._discardChanges();}}
          />
      );
  },
   _AddCancelButton: function(){
      if((_.isEmpty(this.state.changes.update) && this.state.changes.update) && (_.isEmpty(this.state.changes.delete) && this.state.changes.delete) && !this.state.contentDirty){
        var disabled = true;
      }else{
        var disabled = false;
      }

      var className = "btn btn-primary";
      
      return (
          <ClickButton
              title='Cancel'
              className={className}
              isConfirm={true}
              onClick={function(){return location.reload();}}
              isDisabled={disabled}
          />
      );
  },
  _discardChanges(){
    var item = _.cloneDeep(this.state.tmpItem);
    this.setState({
      contentDirty: false,
      tmpItem: {}
    });
    
    $(this.refs.modal).modal('hide');
    

    this._loadContent(item);

  },
  _deleteCallback: function(item){
    var $this = this;

    var deleteObj = this._getObjectById($this.state.appMenuData.children, item.id);

    if(deleteObj){

      //get index
      var deleteIndex = $this._getNodeIndex(item.id, $this.state.appMenuData.children);

      if(deleteObj.new){
        $this.state.appMenuData.children.splice(deleteIndex, 1);

        this._updatePositions(deleteIndex, $this.state.appMenuData.children.length-1, -1);

        //get item from update and remove
        var deleteUpdateIndex = $this._getNodeIndex(item.id, $this.state.changes.update);
        $this.state.changes.update.splice(deleteUpdateIndex, 1);
      }
      else{
        $this.state.changes.delete.push(item);
      }
    }
    else{
      $this.state.changes.delete.push(item);
    }

    $this.setState({
      menuData: $this.state.appMenuData,
    });

  },
  _loadContent(item){
    var $this = this;

    $this.setState({
      loading: true,
      tmpItem: {},
      activeNode: {}
    });

    //figure out if item is new
    if(!item.new){
      var itemCopy = _.cloneDeep(item);
      $.ajax({
        type: 'GET',
        url: Flow.route('flow.node.content.show').replace('{node}', item.id).replace('{lang?}', "en-us"),
        data: {},
        dataType: 'json',
        success(result) {
          itemCopy.content = JSON.parse(result.data.content);
          $this._initActiveNode( itemCopy );
        },
        error:function (xhr, ajaxOptions, thrownError){
          if(xhr.status == 404) {
            itemCopy.content = {};
            $this._initActiveNode( itemCopy );
          }
        }
      });

    }else{
      var itemCopy = _.cloneDeep(item);
      itemCopy.content = {};
      this._initActiveNode( itemCopy );
    }
  },
  _initActiveNode( item ) {

    if ( typeof item.content.player_title == "undefined" ) {
      item.content.player_title = item.outline_title;
    }
    if ( item.type == "html" ) {
       if ( typeof item.content.html == "undefined" ) {
         item.content.html = "";
       }
    }
    this.setState({
      activeNode: item,
      loading: false
    });
  },
  _editCallback: function(item){ 
    var $this = this;

    //if the content is dirty
    if(item.id != this.state.activeNode.id){
      if(this.state.contentDirty){
        this.setState({
          tmpItem: item
        });

        //fire the modal
        $(this.refs.modal).modal('show');
      }else{
        //continue with the action
        this._loadContent(item);
      }
    }
  },
  _renameCallback: function(item, newTitle){

    var renameObj = this._getObjectById(this.state.appMenuData.children, item.id);

    renameObj.changed = true;

    if(this._inArray(this.state.changes.update, item)){
      var updateItem = this._getObjectById(this.state.changes.update, item.id);
      updateItem.outline_title = newTitle;
    }else{
      var updateNode = {
        id: item.id,
        outline_title: newTitle,
      };
      this.state.changes.update.push(updateNode);
    }

    this.setState({
      menuData: this.state.appMenuData,
    });
  },

  _revertCallback: function(item){
    var $this = this;
    var revertItemIndex = this._getNodeByIndex(item, $this.state.changes.delete);
    var pop = $this.state.changes.delete.splice(revertItemIndex, 1);
  },

  _changeActiveContent(content){
    var itemCopy = _.cloneDeep(this.state.activeNode);
    var isChanged = false;
    var oldProp;
    var newProp;
    for( var prop in content ) {
        oldProp = itemCopy.content[prop];
        newProp = content[prop];
        if ( JSON.stringify(oldProp) != JSON.stringify(newProp) ) {
            isChanged = true;
            itemCopy.content[prop] = newProp;
        }
    }
    if ( isChanged ) {
        this.setState({
            activeNode: itemCopy,
            contentDirty: true,
        });
    }
  },

  drop(dropObj, sibling, parentId){

    var $this = this;

    //if(sibling !== dropObj.id){

      //new item is dragged
      if(dropObj.tmpId){

        //get the index of the sibling id
        if(sibling !== 0){
          var siblingIndex = $this._getNodeIndex(sibling, $this.state.appMenuData.children);
            var node = {
              new: true,
              changed: true,
              outline_title: dropObj.id,
              parent_id: parentId,
              position: siblingIndex,
              type: dropObj.id,
              id: dropObj.tmpId,
            }
            $this.state.appMenuData.children.splice(siblingIndex, 0, node);
            $this._updatePositions(siblingIndex+1, $this.state.appMenuData.children.length-1, 1);
            
            $this.state.changes.update.push(node);
        }else{
          //new item at bottom of list

          //if the root has no children
          if(!$this.state.appMenuData.children){
            //need to make new children array
            $this.state.appMenuData.children = [];
          }

          if(!_.isEmpty($this.state.appMenuData.children)){
            var newPos = $this.state.appMenuData.children.length;
          }else{
            var newPos = 0; 
          }

          var node = {
            new: true,
            changed: true,
            outline_title: dropObj.id,
            parent_id: parentId,
            position: newPos,
            type: dropObj.id,
            id: dropObj.tmpId,
          }

          $this.state.appMenuData.children.push(node);
          $this.state.changes.update.push(node);

        }
        
      }else{
        //reorder

        var draggedIndex = $this._getNodeIndex(dropObj.id, $this.state.appMenuData.children);

        if(sibling !== 0){
          var siblingIndex = $this._getNodeIndex(sibling, $this.state.appMenuData.children);
          
          //are we going up or down?
          if(siblingIndex > draggedIndex){
            //down
            var pop = $this.state.appMenuData.children.splice(draggedIndex, 1)
            pop[0].changed = true;
            pop[0].position = siblingIndex-1;
             $this.state.appMenuData.children.splice(siblingIndex-1, 0, pop[0]);
            $this._updatePositions(draggedIndex, siblingIndex-1, -1);    

            //update the dragged item
            if($this._inArray($this.state.changes.update, dropObj)){
              var updateItem = $this._getObjectById($this.state.changes.update, dropObj.id);
              updateItem.position = siblingIndex-1;
            }
            else{
              var updateNode = {
                id: dropObj.id,
                outline_title: dropObj.title,
                position: siblingIndex-1,
              };
              $this.state.changes.update.push(updateNode);
            }

          }
          else if (siblingIndex < draggedIndex){
            //up
            var pop = $this.state.appMenuData.children.splice(draggedIndex, 1);
            pop[0].changed = true;
            pop[0].position = siblingIndex;
            $this.state.appMenuData.children.splice(siblingIndex, 0, pop[0]);
            $this._updatePositions(siblingIndex+1, draggedIndex, -1);    

            //update the dragged item
            if($this._inArray($this.state.changes.update, dropObj)){
              var updateItem = $this._getObjectById($this.state.changes.update, dropObj.id);
              updateItem.position = siblingIndex;
            }
            else{
              var updateNode = {
                id: dropObj.id,
                outline_title: dropObj.title,
                position: siblingIndex,
              };
              $this.state.changes.update.push(updateNode);
            }
          }

        }
        else{
          //dragged to the bottom
          var pop = $this.state.appMenuData.children.splice(draggedIndex, 1);
          pop[0].changed = true;
          pop[0].position = $this.state.appMenuData.children.length - 1;
  
          $this._updatePositions(draggedIndex, $this.state.appMenuData.children.length-1, -1);

          $this.state.appMenuData.children.push(pop[0]);

          if($this._inArray($this.state.changes.update, dropObj)){
            var updateItem = $this._getObjectById($this.state.changes.update, dropObj.id);
            updateItem.position = $this.state.appMenuData.children.length - 1;
          }
          else{
            var updateNode = {
              id: dropObj.id,
              outline_title: dropObj.title,
              position: $this.state.appMenuData.children.length - 1,
            };
            $this.state.changes.update.push(updateNode);
          }
        }
      }
    //}

    //sort
    /*$this.state.appMenuData.children.sort(function(a, b){
      return a.position-b.position;
    });*/

    //sort
    $this.state.changes.update.sort(function(a, b){
      return a.position-b.position;
    });

    $this.setState({
      menuData: $this.state.appMenuData,
    });

  },
  render() {

    var saveButton = this._AddSaveButton();
    var cancelButton = this._AddCancelButton();
    var discardChanges = this._AddDiscardChangesButton();
    //var exportButton = this._AddExportButton();  

    if(this.state.editing){
      var disabled = false;
    }
    else{
      var disabled = true;
    }

    var className = "btn btn-primary";

    return (
      <Loader loading={this.state.loading}>
        <div className="modal fade" role="dialog" ref="modal">
            <div className={'modal-dialog modal-lg'} role="document">
                <div className="modal-content">
                    <div className="modal-header">
                        <button type="button" className="close" data-dismiss="modal" aria-label="Close">&times;</button>
                        <h4 className="modal-title">Save Changes?</h4>
                    </div>
                    <div className="modal-body">
                        You have unsaved changes in this step's Content or Settings, save?
                    </div>
                    <div className="modal-footer">
                      {saveButton}
                      {discardChanges}
                    </div>
                </div>
            </div>
        </div>
        <div className="container-wrapper">
              <div className="container-fluid">
                <div className="row">
                  <div className="col-md-12">
                    <div className="buttons" style={{float: "right", marginBottom: "15px"}}>
                      {saveButton}
                      {cancelButton}
                  </div>
                </div>
              </div>
            </div>
          </div>
        <div style={{position: "relative"}}>
          <NodeToolbar
            data={this.state.toolbarData}
            itemAttributes={Flow.settings.config.NodeTypeMap}
            ref="nodeToolbar"
          />
          <div className="container-wrapper" style={{marginLeft: "80px"}}>
            <div className="container-fluid">
              <div className="row">
                <div className="col-md-4">
                  <Menu 
                    data={this.state.menuData} 
                    itemAttributes={Flow.settings.config.NodeTypeMap}
                    itemDelete={this._deleteCallback}
                    itemRevert={this._revertCallback}
                    itemEdit={this._editCallback}
                    itemRename={this._renameCallback}
                    onItemActive={this._editCallback}
                    activeNode={this.state.activeNode}
                    drop={this.drop}
                    parentId={this.state.menuData.id}
                    ref="menu"
                  />
                </div>
                <div className="col-md-8">
                  <div className={StepEditorStyle.contentcontainer}>
                    <Splitter 
                      direction="vertical"
                      name="ContentSettings"
                      minSizeFirst={50} 
                      minSizeSecond={50}
                      gutterSize={15}
                      containerBackground="#eee"
                      defaultSizeFirst={50}
                      paneBackground="#fff" 
                      obj={this.state.activeNode}
                      onChange={this._changeActiveContent}
                    />
                  </div>
                </div>
            </div>
            </div>
          </div>
        </div>  
      </Loader>
    );
  },
});

module.exports = DragDropContext(HTML5Backend)(FlowBuilder);
