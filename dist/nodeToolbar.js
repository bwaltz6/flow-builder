//React Libraries
var React = require('react');

//Helpers
var _ = require('lodash');
var $ = require('jquery');

//local
var Item = require('./item');
var toolbarStyle = require('./nodeToolbar.css');


var NodeToolbar = React.createClass({
	propTypes: {
        toolbarClass:            React.PropTypes.string,
        nodeClass:              React.PropTypes.string,
    },
    getDefaultProps() {
        return {
        	toolbarClass: '',
        	nodeClass: '',
        }
    },
	getInitialState() {
        return {
            data: this.props.data.data
        };
    },
    render() {

        if(this.props.data && !_.isEmpty(this.props.data)){
       
            var menuClassName = 'list-group ' + toolbarStyle.toolbar;
            var nodes = [];

            var itemAttributes = this.props.itemAttributes;

            for(var i = 0; i < this.props.data.data.length; i++){
                for(var k = 0; k < this.props.data.data[i].types.length; k++){
                    var nodeTitle = this.props.data.data[i].types[k].name;
                    nodes.push(
                        <Item 
                            itemAttributes={itemAttributes} 
                            nodeTitle={nodeTitle} 
                            key={nodeTitle} 
                            id={nodeTitle}
                        />
                    );
                }
            }
        } 
        return (
            <ul className={menuClassName} ref="nodeToolbar"> 	
	        	{nodes}
            </ul>
        );
    },
});


module.exports = NodeToolbar;
