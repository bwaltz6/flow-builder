//React Libraries
var React = require('react');
var ReactDOM = require('react-dom');

//Helpers
var _ = require('lodash');
var $ = require('jquery');

//Local
var menuStyle = require('./menu.css');
var NodeItem = require('./nodeItem');
var ItemTypes = require('./Constants').ItemTypes;
var DropTarget = require('react-dnd').DropTarget;

var menuTarget = {
  drop: function (props, monitor, component) {
    props.drop(monitor.getItem(), component.state.placeholderPosition, props.parentId);
    component._resetPlaceholder();
  },
  hover: function(props, monitor, component){

    var height = monitor.getItem().height;
    component._changePlaceholderHeight(height);

    // Determine rectangle on screen
    var containerBoundingRect = ReactDOM.findDOMNode(component).getBoundingClientRect();

    // Determine mouse position
    var clientOffset = monitor.getClientOffset();

    if (clientOffset.y >= (containerBoundingRect.bottom - 50)) {
        //withing 50px of bottom, lets scroll
        component._scrollDragHeight(height);
    }
  }
};

function collect(connect, monitor) {
  return {
    connectDropTarget: connect.dropTarget(),
    isOver: monitor.isOver()
  };
}

var Menu = React.createClass({
    propTypes: {
        isOver: React.PropTypes.bool.isRequired
    },
    getInitialState() {
        return {
            placeholderPosition: 0,
            placeholderHeight: 50,
            activeNode: 0,
            renameNodes: [],
        };
    },
    _movePlaceholderBefore: function(id){
        this.setState({
            placeholderPosition: id,
        });
    },
    _movePlaceholderAfter: function(id){
        var index = _.findIndex(this.props.data.children, {id: id});
        var next = this.props.data.children[index + 1];
        var placeholderPosition = next ? next.id : 0;

        this.setState({
            placeholderPosition: placeholderPosition,
        });
    },
    _resetPlaceholder(){
        this.setState({placeholderPosition: 0});
    },
    _changePlaceholderHeight(height) {
        this.setState({
            placeholderHeight: height,
        });
    },
    _scrollDragHeight(height){
        //handle scrolling to end of container
        this.refs.courseOutline.scrollTop += $(this.refs.courseOutline)[0].scrollHeight;
    },
    setActiveNode(item){

        //if we have a tmpId use it...else we know we have id
        if(item.tmpId){
            this.setState({
                activeNode: item.tmpId
            }); 
        }
        else{
            this.setState({
                activeNode: item.id
            }); 
        }
    },
    itemEdit(item){
        //this.setActiveNode(item);
        this.props.itemEdit(item);
    },
    handleNodeRenaming(renameItem){
        if(_.isEmpty(this.state.renameNodes) || !$.inArray(renameItem.id, this.state.renameNodes)){
            var newRenameNodes = _.cloneDeep(this.state.renameNodes);
            newRenameNodes.push(renameItem.id);

            this.setState({
                renameNodes: newRenameNodes
            });
        }
    },
    renameRemove(renameItem){
        var newRenameNodes = _.cloneDeep(this.state.renameNodes);
        newRenameNodes = _.pull(newRenameNodes, renameItem.id);
        this.setState({
            renameNodes: newRenameNodes
        });
    },
    renameSubmit(renameItem, newTitle){
        
        this.renameRemove(renameItem);
        this.props.itemRename(renameItem, newTitle);
    },
    render() {

        var connectDropTarget = this.props.connectDropTarget;
        var isOver = this.props.isOver;
        var $this = this;

        var parentId = $this.props.data.id;
        var nodeClass = 'list-group-item ' +this.props.nodeClass;

        if($this.props.data && !_.isEmpty($this.props.data)){
            if($this.props.data.children){
                var items = [];
                var i = 1;
                $this.props.data.children.forEach(function(item) {

                    if(this.props.isOver && item.id === this.state.placeholderPosition){
                        items.push(<li className={menuStyle.placeholder} style={{height: this.state.placeholderHeight}} key="placeholder"></li>);
                    }

                    var active = item.id == this.props.activeNode.id ? true : false;

                    items.push(
                            <NodeItem
                                index={i}
                                key={item.id} 
                                itemAttributes={this.props.itemAttributes} 
                                item={item} 
                                id={item.id}
                                onDelete={this.props.itemDelete}
                                onDeleteRevert={this.props.itemRevert}
                                onEdit={this.itemEdit}
                                onRenameSubmit={this.renameSubmit}
                                onRenameCancel={this.renameRemove}
                                onRenaming={this.handleNodeRenaming}
                                active={active}
                                movePlaceholderBefore={$this._movePlaceholderBefore}
                                movePlaceholderAfter={$this._movePlaceholderAfter}
                            />
                    );

                    i++;
                }.bind(this));

                if (this.props.isOver && this.state.placeholderPosition === 0) {
                    items.push(<li className={menuStyle.placeholder} style={{height: this.state.placeholderHeight}} key="placeholder"></li>);
                }
            }else{
                var items = [];
            }
        }

        //class names
        var menuClassName = 'list-group ' + menuStyle.menu;
        var headerClassName = menuStyle.menuHeader + ' ' + this.props.headerClass;

        if(!_.isEmpty(this.state.renameNodes)){
            return (
                <div className={this.props.containerClass}>
                    
                    <div className={menuStyle.menuwrapper} id={menuStyle.menu} >
                        
                        <ul ref="courseOutline" className={menuClassName} data-nodeid={parentId} >
                            {items}
                        </ul>
                        
                    </div>

                </div>
            )
        }else{
            return connectDropTarget(
                <div className={menuStyle.container}>
                    
                    <div className={menuStyle.menuwrapper} id={menuStyle.menu} >
                        
                        <ul ref="courseOutline" className={menuClassName} data-nodeid={parentId} >
                            {items}
                        </ul>
                        
                    </div>

                </div>
            )
        }
        
    },
});

module.exports = DropTarget(ItemTypes.NODEITEM, menuTarget, collect)(Menu);