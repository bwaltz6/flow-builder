var React = require('react');
var ReactDOM = require('react-dom');
var _ = require('lodash');
var $ = require('jquery');

var HtmlStyle = require('./html.css');

var Html = React.createClass({
  render() {
   return(
      <div className={HtmlStyle.step_play_html} dangerouslySetInnerHTML={{__html: this.props.content.html}}>
      </div>
    );
  },
});


module.exports = Html;
