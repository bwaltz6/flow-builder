var React = require('react');
var ReactDOM = require('react-dom');
var _ = require('lodash');
var $ = require('jquery');

var IframeStyle = require('./iframe.css');

var Iframe = React.createClass({
  render() {
   return(
      <div className={IframeStyle.play_step_iframe} dangerouslySetInnerHTML={{__html: this.props.content.iframe}}>
      </div>
    );
  },
});


module.exports = Iframe;
