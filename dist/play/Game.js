var React = require('react');
var ReactDOM = require('react-dom');
var _ = require('lodash');
var $ = require('jquery');

var QuizshowGame = require('./QuizshowGame.js');
var ZoouGame = require('./ZoouGame.js');

var Game = React.createClass({
  render() {
   return(
     <div>
        <div style={{display: (this.props.content.gameType=="quizshow") ? "block" : "none" }}>
          <QuizshowGame
            content={this.props.content}
          />
        </div>
        <div style={{display: (this.props.content.gameType=="zoou") ? "block" : "none" }}>
          <ZoouGame
            content={this.props.content}
          />
        </div>
     </div>
    );
  },
});

/*
*/

module.exports = Game;
