var React = require('react');
var _ = require('lodash');
var $ = require('jquery');

var StepEditorStyle = require('./stepEditor.css');

var Splitter = require('./splitter/splitter.js');

var StepEditor = React.createClass({
	propTypes: {
        onSubmit: React.PropTypes.func,
    },
    getInitialState() {
        return {
            serverObj: this.props.activeObj
        };
    },
    render() {
        return(
            <div className={StepEditorStyle.contentcontainer}>
                <Splitter direction="vertical" 
                minSizeFirst={50} 
                minSizeSecond={50}
                gutterSize={15}
                containerBackground="#eee"
                defaultSizeFirst={50}
                paneBackground="#fff" 
                obj={this.props.activeObj}
                //onSubmit={this.props.onSubmit}
                />
            </div>
        );
    },
});


module.exports = StepEditor;
